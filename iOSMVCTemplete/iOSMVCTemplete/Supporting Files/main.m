//
//  main.m
//  iOSTemplete
//
//  Created by Maktub on 01/05/2017.
//  Copyright © 2017 maktub. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
