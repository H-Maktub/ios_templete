//
//  UIImageView+MTWebImage.h
//  onemoney
//
//  Created by Maktub on 16/3/3.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (MTWebImage)

-(void)mt_setImageWithUrlString:(NSString *)urlstring;
-(void)mt_setRetSetImageWithUrlString:(NSString *)urlstring;
//-(void)mt_setImageWithFtp:(NSString *)urlstring fileName:(NSString *)fileName;
@end
