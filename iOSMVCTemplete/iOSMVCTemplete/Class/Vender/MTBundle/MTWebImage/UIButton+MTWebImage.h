//
//  UIButton+MTWebImage.h
//  onemoney
//
//  Created by Maktub on 16/3/5.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (MTWebImage)

- (void)mt_setBackgroundImageWithURL:(NSString *)urlstring forState:(UIControlState)state;
- (void)mt_setImageWithURL:(NSString *)urlstring forState:(UIControlState)state;
@end
