//
//  MTNavigationController.m
//  YingCaa
//
//  Created by interest on 16/4/26.
//  Copyright © 2016年 interest. All rights reserved.
//

#import "MTNavigationController.h"
#import "UIBarButtonItem+MTButton.h"

@interface MTNavigationController ()<
UINavigationControllerDelegate
>

/**
 *  用于保存协议
 */
@property(nonatomic, strong) id popDeletge;

@end

@implementation MTNavigationController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _popDeletge = self.interactivePopGestureRecognizer.delegate;
    
    self.delegate = self;
    
    //设置字体
    [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:17.0f]}];

    self.navigationBar.translucent = NO;
//    self.navigationBar.barTintColor = ELMaincolor;
    
    //下划线隐藏
//    [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny  barMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [[UIImage alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
//    [kHMCoreDataManager.managedObjectContext reset];
    if (self.childViewControllers.count) { // 不是根控制器
        viewController.hidesBottomBarWhenPushed = YES;
        // 设置导航条的按钮
        UIBarButtonItem *popPreVc = [UIBarButtonItem mt_barButtonItemWithImage:@"返回" highImage:@"返回" target:self action:@selector(popToPreVc)];
        viewController.navigationItem.leftBarButtonItem = popPreVc;
    }
    [super pushViewController:viewController animated:animated];
}

//- (void)mt_pushViewController:(UIViewController *)viewController animated:(BOOL)animated
//{
//    
//    if ([MTUserCenter sharedInstance].islogin) {
//        [self pushViewController:viewController animated:animated];
//    }else{
//        LoginController *vc = [[LoginController alloc] init];
//        MTNavigationController *nav = [[MTNavigationController alloc] initWithRootViewController:vc];
//        [MTKeyWindows.rootViewController presentViewController:nav animated:YES completion:^{
//            
//        }];
//    }
//    
//}
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (viewController == self.viewControllers[0]) {
        self.interactivePopGestureRecognizer.delegate = _popDeletge;
        
        
    }
    else{
        self.interactivePopGestureRecognizer.delegate = nil;
    }
}
- (void)popToPreVc
{
//    [kHMCoreDataManager.managedObjectContext reset];
    [self popViewControllerAnimated:YES];
}
//用子子控制器设置标题栏颜色
//-(UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
- (UIViewController *)childViewControllerForStatusBarStyle{
    return self.topViewController;
}

@end
