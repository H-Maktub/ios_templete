//
//  MTHttpTool.h
//  onemoney
//
//  Created by Maktub on 15/12/21.
//  Copyright © 2015年 Maktub-小明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTUploadParam.h"

@interface MTHttpTool : NSObject

/**
 *  get请求
 *
 *  @param URLString  请求连接
 *  @param parameters 参数
 *  @param success    成功
 *  @param failure    失败
 */
+ (void)get:(NSString *)URLString
 parameters:(NSObject *)parameters
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSError *error))failure;

/**
 *  post请求
 *
 *  @param URLString  请求连接
 *  @param parameters 请求参数
 *  @param success    成功
 *  @param failure    失败
 */
+ (void)post:(NSString *)URLString
  parameters:(NSObject *)parameters
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSError *error))failure;

/**
 *  上传
 *
 *  @param URLString   上传连接
 *  @param parameters  参数
 *  @param uploadParam 上传参数
 *  @param success     成功
 *  @param failure     失败
 */
+ (void)upload:(NSString *)URLString
    parameters:(NSObject *)parameters
   uploadParam:(NSArray<MTUploadParam *>*)uploadParam
       success:(void (^)(id responseObject))success
       failure:(void (^)(NSError *))failure;

@end
