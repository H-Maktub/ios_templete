//
//  NSDate+RECurrentDate.h
//  Recruitment
//
//  Created by Maktub on 12/04/2017.
//  Copyright © 2017 Maktub. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (RECurrentDate)
+(NSString *)mt_currentDay:(NSInteger)day;

@end
