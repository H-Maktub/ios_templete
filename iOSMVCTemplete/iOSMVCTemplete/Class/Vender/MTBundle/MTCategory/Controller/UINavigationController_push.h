//
//  UINavigationController_push.h
//  Recruitment
//
//  Created by Maktub on 14/04/2017.
//  Copyright © 2017 Maktub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController ()
- (void)mt_pushViewController:(UIViewController *)viewController animated:(BOOL)animated;
@end
