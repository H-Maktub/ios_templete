//
//  UIViewController+Param.m
//  onemoney
//
//  Created by Maktub on 16/3/3.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UIViewController+MTParam.h"

#import <objc/runtime.h>

@implementation UIViewController (MTParam)

static const char MTParamKey = '\0';
- (void)setMt_param:(NSObject *)mt_param
{
    if (mt_param != self.mt_param) {
        objc_setAssociatedObject(self, &MTParamKey,mt_param, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (NSObject *)mt_param
{
    return objc_getAssociatedObject(self, &MTParamKey);
}
@end
