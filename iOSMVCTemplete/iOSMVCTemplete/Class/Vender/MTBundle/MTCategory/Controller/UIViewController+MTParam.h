//
//  UIViewController+Param.h
//  onemoney
//
//  Created by Maktub on 16/3/3.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MTParam)

@property (nonatomic ,strong) NSObject *mt_param;

@end
