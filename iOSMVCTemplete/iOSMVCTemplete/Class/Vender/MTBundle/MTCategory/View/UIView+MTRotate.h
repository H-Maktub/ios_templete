//
//  UIView+MTRotate.h
//  YingCaa
//
//  Created by Maktub on 8/15/16.
//  Copyright © 2016 interest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MTRotate)
- (void)mt_RotateToLandscapeRight;
- (void)mt_RotateToLandscapeLeft;
@end
