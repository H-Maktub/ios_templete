//
//  UIView+MTRotate.m
//  YingCaa
//
//  Created by Maktub on 8/15/16.
//  Copyright © 2016 interest. All rights reserved.
//

#import "UIView+MTRotate.h"

@implementation UIView (MTRotate)

- (void)mt_RotateToLandscapeRight{
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
    self.transform = CGAffineTransformMakeRotation(M_PI/2);
    self.bounds = CGRectMake(0, 0, self.frame.size.height, self.frame.size.width);
}

- (void)mt_RotateToLandscapeLeft{
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeLeft;
    self.transform = CGAffineTransformMakeRotation(-M_PI/2);
    self.bounds = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}
@end
