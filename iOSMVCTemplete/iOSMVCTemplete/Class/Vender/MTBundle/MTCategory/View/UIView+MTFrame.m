//
//  UIView+MTFrame.m
//  YingCaa
//
//  Created by interest on 16/4/26.
//  Copyright © 2016年 interest. All rights reserved.
//

#import "UIView+MTFrame.h"

@implementation UIView (MTFrame)


//设置宽度
-(CGFloat)mt_width{
    return self.frame.size.width;
}
-(void)setMt_width:(CGFloat)mt_width{
    CGRect frame = self.frame;
    frame.size.width = mt_width;
    self.frame = frame;
}
//设置高度
-(CGFloat)mt_height{
    return self.frame.size.height;
}
-(void)setMt_height:(CGFloat)mt_height{
    CGRect frame = self.frame;
    frame.size.height = mt_height;
    self.frame = frame;
}
//设置x坐标
-(CGFloat)mt_x{
    return self.frame.origin.x;
}
-(void)setMt_x:(CGFloat)mt_x{
    CGRect frame = self.frame;
    frame.origin.x = mt_x;
    self.frame = frame;
}
//设置y坐标
-(CGFloat)mt_y{
    return self.frame.origin.y;
}
-(void)setMt_y:(CGFloat)mt_y{
    CGRect frame = self.frame;
    frame.origin.y = mt_y;
    self.frame = frame;
}
//设置平度大小
-(CGSize)mt_size{
    return self.frame.size;
}
-(void)setMt_size:(CGSize)mt_size{
    CGRect frame = self.frame;
    frame.size = mt_size;
    self.frame = frame;
}
@end
