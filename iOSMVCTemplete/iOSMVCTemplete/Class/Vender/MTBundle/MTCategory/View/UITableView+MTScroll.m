//
//  UITableView+MTScroll.m
//  onemoney
//
//  Created by Maktub on 16/3/28.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UITableView+MTScroll.h"

@implementation UITableView (MTScroll)

- (void)mt_scrollToTop {
    
    [self setContentOffset:CGPointMake(0,0) animated:NO];
}

- (void)mt_scrollToBottom {
    
    NSUInteger sectionCount = [self numberOfSections];
    if (sectionCount) {
        
        NSUInteger rowCount = [self numberOfRowsInSection:0];
        if (rowCount) {
            
            NSUInteger indexes[2] = {0, rowCount - 1};
            NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
            [self scrollToRowAtIndexPath:indexPath
                                  atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
    }    
}

@end
