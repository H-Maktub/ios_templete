//
//  UITextField+MTLeftRight.m
//  onemoney
//
//  Created by Maktub on 16/3/21.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UITextField+MTLeftRight.h"
#import "UIView+MTFrame.h"

@implementation UITextField (MTLeftRight)

- (void)mt_setLeftViewWithImage:(UIImage *)image{
    self.font = [UIFont systemFontOfSize:14];
    self.textColor = [UIColor darkGrayColor];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.mt_width += 30;
    imageView.contentMode = UIViewContentModeCenter;
    self.leftView =imageView;
    self.leftViewMode = UITextFieldViewModeAlways;

    
}

- (void)mt_setLeftViewWithSearchImage:(UIImage *)image{
    self.font = [UIFont systemFontOfSize:14];
    self.textColor = [UIColor whiteColor];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.mt_width += 15;
    imageView.contentMode = UIViewContentModeCenter;
    self.leftView =imageView;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    
}

@end
