//
//  UIView+OMReLoadDate.h
//  onemoney
//
//  Created by Maktub on 16/3/5.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MTReLoadDate)
-(void)mt_reLoadData;
@end
