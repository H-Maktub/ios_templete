//
//  UIView+Vc.h
//  onemoney
//
//  Created by Maktub on 16/1/12.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MTGetVc)

- (UIViewController *)mt_getCurrentController:(Class)Vc;

@end
