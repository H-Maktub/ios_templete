//
//  NSString+MTTime.m
//  onemoney
//
//  Created by Maktub on 16/3/11.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "NSString+MTTime.h"

@implementation NSString (MTTime)

+(instancetype)mt_stringFromDateWithMilliSec:(long long)time{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss:SSS"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)time / 1000]];
    
    return currentDateStr;
}

@end
