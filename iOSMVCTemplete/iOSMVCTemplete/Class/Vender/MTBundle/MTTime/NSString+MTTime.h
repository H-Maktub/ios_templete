//
//  NSString+MTTime.h
//  onemoney
//
//  Created by Maktub on 16/3/11.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MTTime)

+(instancetype)mt_stringFromDateWithMilliSec:(long long)time;

@end
