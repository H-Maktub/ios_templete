//
//  NSObject+MTKeyValue.h
//  onemoney
//
//  Created by Maktub on 16/3/3.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (MTKeyValue)

- (NSMutableDictionary *)mt_keyValues;
+ (instancetype)mt_objectWithKeyValues:(id)keyValues;

/**
 *  解码（从文件中解析对象）
 */
- (void)mt_decode:(NSCoder *)decoder;
/**
 *  编码（将对象写入文件中）
 */
- (void)mt_encode:(NSCoder *)encoder;

/**
 *  更新模型
 *
 *  @param model 模型
 */
- (void)mt_setPropertyWithModel:(NSObject *)model;
@end

/**
 归档的实现
 */
#define MTCodingImplementation \
- (id)initWithCoder:(NSCoder *)decoder \
{ \
if (self = [super init]) { \
[self mt_decode:decoder]; \
} \
return self; \
} \
\
- (void)encodeWithCoder:(NSCoder *)encoder \
{ \
[self mt_encode:encoder]; \
}
