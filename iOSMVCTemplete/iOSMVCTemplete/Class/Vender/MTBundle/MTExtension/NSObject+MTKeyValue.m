//
//  NSObject+MTKeyValue.m
//  onemoney
//
//  Created by Maktub on 16/3/3.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "NSObject+MTKeyValue.h"
#import <MJExtension/MJExtension.h>

@implementation NSObject (MTKeyValue)

- (NSMutableDictionary *)mt_keyValues
{
    return self.mj_keyValues;
}

+ (instancetype)mt_objectWithKeyValues:(id)keyValues{
    return [self mj_objectWithKeyValues:keyValues];
}

/**
 *  解码（从文件中解析对象）
 */
- (void)mt_decode:(NSCoder *)decoder{
    [self mj_decode:decoder];
}
/**
 *  编码（将对象写入文件中）
 */
- (void)mt_encode:(NSCoder *)encoder{
    [self mj_encode:encoder];
}

/**
 *  获取类的全部属性
 *
 *  @return 类全部属性的NSSet
 */
+ (instancetype)propertyArray{
    __block NSMutableSet *set = [NSMutableSet set];
    [self mj_enumerateProperties:^(MJProperty *property, BOOL *stop) {
        [set addObject:property.name];
    }];
    return [set copy];
}

- (void)mt_setPropertyWithModel:(NSObject *)model{
    if (nil != model) {
        NSMutableSet *selfSet = [[[self class] propertyArray] mutableCopy];
        NSSet *modelSet = [[model class] propertyArray];
        [selfSet intersectSet:modelSet];
        [selfSet enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
            NSObject *aModel = [model valueForKey:obj];
            NSSet *aModelSet = [[aModel class] propertyArray];
            if (0 != aModelSet.count) {
                if (![self valueForKey:obj]) {
                    [self setValue:[[NSClassFromString(obj) alloc] init] forKey:obj];
                }
                [[self valueForKey:obj] mt_setPropertyWithModel:aModel];
            }else{
                [self setValue:aModel forKey:obj];
            }
        }];
    }
}
@end
