//
//  UIScrollView+MTRefresh.m
//  onemoney
//
//  Created by Maktub on 16/3/14.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UIScrollView+MTRefresh.h"
#import <MJRefresh/MJRefresh.h>

@implementation UIScrollView (MTRefresh)

- (void)mt_headerRefreshWithBlock:(void (^)(void))block{
    [self mt_headerRefreshWithContentInsetTop:0 Block:block];
}


- (void)mt_headerRefreshWithContentInsetTop:(CGFloat)num Block:(void (^)(void))block{
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if (block) {
            block();
        }
    }];
    header.ignoredScrollViewContentInsetTop = num;
    self.mj_header = header;
}


- (void)mt_footerRefreshWithBlock:(void (^)(void))block{
    [self mt_footerRefreshWithBlock:block ContentInsetBottom:0];
}

- (void)mt_footerRefreshWithBlock:(void (^)(void))block ContentInsetBottom:(CGFloat)num{
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        if (block) {
            block();
        }
    }];
    footer.ignoredScrollViewContentInsetBottom = num;
    footer.automaticallyHidden = YES;
    self.mj_footer = footer;
}

- (void)mt_endRefreshing{
    if (self.mj_header.isRefreshing) {
        [self.mj_header endRefreshing];
    };
    if (self.mj_footer.isRefreshing) {
        [self.mj_footer endRefreshing];
    };
}
-(void)mt_headerBeginRefreshing{
    if (self.mj_header.isRefreshing) return;
    [self.mj_header beginRefreshing];
}
-(void)mt_footerBeginRefreshing{
    if (self.mj_footer.isRefreshing) return;
    [self.mj_footer beginRefreshing];
}

/** 提示没有更多的数据 */
- (void)mt_setNoMoreData{
    if (self.mj_footer != NULL) {
        [self.mj_footer endRefreshingWithNoMoreData];
    }
}

/** 重置没有更多的数据（消除没有更多数据的状态） */
- (void)mt_resetNoMoreData{
    if (self.mj_footer != NULL) {
        [self.mj_footer resetNoMoreData];
    }
}
@end
