//
//  MTUserCenter.h
//  YingCaa
//
//  Created by interest on 16/4/29.
//  Copyright © 2016年 interest. All rights reserved.
//

#import <Foundation/Foundation.h>

//model
#import "MTUserModel.h"

@interface MTUserCenter : NSObject

@property (nonatomic, strong) MTUserModel *user;

//是否已经登录
@property (nonatomic, readonly, assign) BOOL islogin;

//单例
+ (instancetype)sharedInstance;

/**
 *  更新用户信息
 *
 *  
 */
+ (void)upDataUserWithModel:(NSObject *)model;

//关闭
+ (void)tearDown;

+(void)save;

@end
