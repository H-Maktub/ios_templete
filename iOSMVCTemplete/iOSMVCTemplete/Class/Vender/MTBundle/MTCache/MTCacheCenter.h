//
//  MTCacheCenter.h
//  YingCaa
//
//  Created by interest on 16/4/29.
//  Copyright © 2016年 interest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTUserModel.h"

@interface MTCacheCenter : NSObject

+ (instancetype)sharedInstance;
- (void)yc_cacheWithUser:(MTUserModel *)user;
- (MTUserModel *)yc_getCacheUser;

@end
