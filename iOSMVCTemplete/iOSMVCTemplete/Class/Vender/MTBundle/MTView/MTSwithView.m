//
//  MTSwithView.m
//  iOSTemplete
//
//  Created by Maktub on 13/07/2017.
//  Copyright © 2017 maktub. All rights reserved.
//

#import "MTSwithView.h"

@interface MTSwithView()

@property (nonatomic, strong) NSMutableArray<UIButton *> *btnArray;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, weak) id target;
@property (nonatomic, assign) SEL action;
@end

@implementation MTSwithView


-(instancetype)initWithArray:(NSArray *)titleArray{
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        _btnArray = [NSMutableArray array];
        float number = titleArray.count;
        [titleArray enumerateObjectsUsingBlock:^(NSString  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn addTarget:self action:@selector(changeBtn:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = 10000+idx;
//            btn.titleLabel.font = ELMainFont;
//            btn.titleLabel.textColor = ELFontColor;
            [btn setTitle:titleArray[idx] forState:UIControlStateNormal];
//            [btn setTitleColor:ELMaincolor forState:UIControlStateSelected];
//            [btn setTitleColor:ELFontColor forState:UIControlStateNormal];
            [self addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.equalTo(self);
                make.width.equalTo(self).dividedBy(titleArray.count);
                make.right.equalTo(self.mas_right).multipliedBy((idx+1) /number);
            }];
            [_btnArray addObject:btn];
        }];
        
        
        
        _btnArray[0].selected = YES;
        UIView *bottomView = [[UIView alloc] init];
        _bottomView = bottomView;
//        bottomView.backgroundColor = ELMaincolor;
        [self addSubview:bottomView];
        [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.centerX.equalTo(_btnArray[0]);
            make.width.equalTo(_btnArray[0]);
            make.height.equalTo(@3);
            
        }];
        
        
    }
    return self;
}

- (void)changeBtn:(UIButton *)btn{
    if (!_reload) {
        if (btn.selected) {
            return;
        }
    }

    [_btnArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.selected = NO;
    }];
    btn.selected = YES;
    
    [_bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.centerX.equalTo(btn);
        make.height.equalTo(@3);
        make.width.equalTo(btn);
    }];
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
    }];
    
    _index = btn.tag -10000;
    
    
    if(_target != nil &&  _action != nil && [_target respondsToSelector:_action]){
        [_target performSelector:_action withObject:self afterDelay:0.0];
    }
    
    
    
}

- (void)addTarget:(nullable id)target action:(SEL)action{
    
    _target = target;
    _action = action;
}

@end
