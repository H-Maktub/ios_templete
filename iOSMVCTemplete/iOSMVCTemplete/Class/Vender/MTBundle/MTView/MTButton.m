//
//  MTButton.m
//  iOSTemplete
//
//  Created by Maktub on 07/07/2017.
//  Copyright © 2017 maktub. All rights reserved.
//

#import "MTButton.h"

@implementation MTButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)layoutSubviews {
    [super layoutSubviews];
    
    // Center image
    CGPoint imageCenter = self.imageView.center;
    imageCenter.x = self.frame.size.width/2;
    imageCenter.y = self.imageView.frame.size.height/2;
    self.imageView.center = imageCenter;
    
    //Center text
    CGRect newFrame = [self titleLabel].frame;
    newFrame.origin.x = 0;
    newFrame.origin.y = self.imageView.frame.size.height + 5;
    newFrame.size.width = self.frame.size.width;
    
    self.titleLabel.frame = newFrame;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    //center all
    float height = self.mt_height - self.titleLabel.mt_y - self.titleLabel.mt_height;
    CGPoint iCenter = self.imageView.center;
    iCenter.y += height/2.0 ;
    self.imageView.center = iCenter;
    
    CGPoint tCenter = self.titleLabel.center;
    tCenter.y += height/2.0 ;
    self.titleLabel.center = tCenter;
    
}

@end
