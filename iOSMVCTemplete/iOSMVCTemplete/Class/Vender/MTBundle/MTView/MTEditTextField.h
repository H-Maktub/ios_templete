//
//  MTEditTextField.h
//  iOSTemplete
//
//  Created by Maktub on 2017/12/10.
//  Copyright © 2017年 maktub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTEditTextField : UIView


@property (nonatomic, strong) UITextField *textField;
- (void)setTitle:(NSString *)text font:(UIFont *)font;

@end
