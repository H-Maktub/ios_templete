//
//  MTSelectRoot.h
//  Recruitment
//
//  Created by Maktub on 03/04/2017.
//  Copyright © 2017 Maktub. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTSelectRoot : NSObject
+(void)mt_chooseRootViewController:(UIWindow *)window;
@end
